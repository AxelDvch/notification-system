package fr.axel;

public class App {

    public static void main(String[] args) {
        Flight parisAuckland = new Flight("Auckland", 10, "Paris", 20.0);
        Flight aucklandSydney = new Flight("Sydney", 10, "Auckland", 20.0);

        Customer melissa = new Customer("8 rue pot de fleur", "melissa@email.com", "Melissa");
        parisAuckland.reserveASeat(melissa, 1);

        Customer jean = new Customer("1 rue des olivier", "jean@email.com", "Jean");
        FlightTicket ticket1 = parisAuckland.reserveASeat(jean, 2);

        Customer etienne = new Customer("256 rue de vinci", "etienne@email.com", "Etienne");
        FlightTicket ticket2 = aucklandSydney.reserveASeat(etienne, 1);
        FlightTicket ticket3 = aucklandSydney.reserveASeat(jean, 2);

        //Shoud send a new notification cycle
        parisAuckland.setPrice(25.0);
    }

}
