package fr.axel;

import java.util.ArrayList;
import java.util.List;

public class Customer implements Observer {

    private final String address;
    private final String name;
    private final List<NotificationSystem> subscribes;

    public Customer(String address, String email, String name) {
        this.address = address;
        this.name = name;
        this.subscribes = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    @Override
    public void subscribe(NotificationSystem notificationSystem) {
        this.subscribes.add(notificationSystem);
        notificationSystem.subscribe(this, NotificationSystem::logCustomerNotification);
    }

    @Override
    public void unsubscribe(NotificationSystem notificationSystem) {
        this.subscribes.remove(notificationSystem);
        notificationSystem.unsubscribe(this);
    }

    @Override
    public String toString() {
        return "Customer: " + "name = " + name;
    }

}
