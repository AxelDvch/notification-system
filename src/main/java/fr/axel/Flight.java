package fr.axel;

import fr.axel.exception.FlyFullException;
import fr.axel.exception.SeatNotAvailableException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class Flight {

    private final String arrivalAirport;
    private final String departAirport;
    private final NotificationSystem notificationSystem;
    private final Map<Integer, Customer> seats;

    private int availablePlaces;
    private double price;

    public Flight(String arrivalAirport, int availablePlaces, String departAirport, double price) {
        this.arrivalAirport = arrivalAirport;
        this.availablePlaces = availablePlaces;
        this.departAirport = departAirport;
        this.notificationSystem = new NotificationSystem();
        this.price = price;
        this.seats = new HashMap<>(availablePlaces);
    }

    /***
     * Reserve a seat and subscribe to the notification system of the flight
     * Send a new notification to the system
     * @param customer
     * @param seatNumber
     * @return flyTicket
     */
    public FlightTicket reserveASeat(Customer customer, Integer seatNumber) {
        try {
            if (flightIsFull()) {
                throw new FlyFullException();
            }

            if (this.seats.put(seatNumber, customer) == null) {
                --this.availablePlaces;

                notifyCustomer("Available places : " + this.availablePlaces);
                customer.subscribe(notificationSystem);
            } else {
                throw new SeatNotAvailableException();
            }
        } catch (FlyFullException | SeatNotAvailableException e) {
            e.printStackTrace();
            return null;
        }
        return new FlightTicket();
    }

    /***
     * Send a new notification to the system
     * @param price price of the flight
     */
    public void setPrice(double price) {
        this.price = price;
        this.notifyCustomer("Updated price : " + this.price);
    }

    private boolean flightIsFull() {
        return this.availablePlaces == 0;
    }

    private void notifyCustomer(String message) {
        String voyage = this.departAirport + " -> " + this.arrivalAirport;
        String notification = """
                Notification flight: %s,
                Time : %s,
                %s
                """.formatted(voyage,
                              LocalDateTime.now()
                                           .format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")),
                              message);

        this.notificationSystem.notifySubscribers(notification);
    }

}
