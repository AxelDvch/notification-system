package fr.axel;

import java.util.UUID;

public class FlightTicket {

    private final UUID ticketNumber;

    public FlightTicket() {
        this.ticketNumber = UUID.randomUUID();
    }

    public UUID getTicketNumber() {
        return ticketNumber;
    }
}
