package fr.axel;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class NotificationSystem {

    private final Map<Object, BiConsumer<Observer, Object>> listeners = new HashMap<>();

    public static void logCustomerNotification(Observer customer, Object event) {
        System.out.println(customer + "\n" + event);
    }

    public void notifySubscribers(Object event) {
        System.out.println("#### NOTIFICATION CYCLE ####\n");
        listeners.forEach((k, v) -> v.accept((Observer) k, event));
    }

    public void subscribe(Observer observer, BiConsumer<Observer, Object> listener) {
        listeners.put(observer, listener);
    }

    public void unsubscribe(Observer observer) {
        listeners.remove(observer);
    }

}
