package fr.axel;

public interface Observer {

    void subscribe(NotificationSystem notificationSystem);

    void unsubscribe(NotificationSystem notificationSystem);
    
}
