package fr.axel.exception;

public class FlyFullException extends RuntimeException {

    public FlyFullException() {
        super("L'avion est plein");
    }

}
