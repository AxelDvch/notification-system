package fr.axel.exception;

public class ObserverNotSubscribeException extends RuntimeException {

    public ObserverNotSubscribeException() {
        super("L'observer founit n'est pas abonné au systeme de notification");
    }

}
