package fr.axel.exception;

public class SeatNotAvailableException extends RuntimeException {

    public SeatNotAvailableException() {
        super("La place réservé n'est pas disponible");
    }

}
